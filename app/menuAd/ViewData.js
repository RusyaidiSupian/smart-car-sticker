import React, { Component } from 'react';
import { Text, View, StyleSheet, FlatList } from 'react-native';

export default class ViewData extends Component {
    constructor() {
        super()
        this.state = {
            dataSource: [],
            //refreshing: false,
        }
    }
    //custom the layout of flatlist here
    renderItem = ({ item }) => {
        return (
            <View style={styles.container}>
                <View style={styles.container1}>
                <Text style={styles.buttonText}> List of Resident {item.id} </Text>
                    <Text>Username: {item.username}</Text>
                    <Text>Name: {item.name}</Text>
                    <Text>Contact: {item.contact}</Text>
                    <Text>Car Number: {item.no_plat}</Text>
                    <Text>{item.resident_status}</Text>
                    <Text>Date Enrollment: {item.date_enrol}</Text>
                    {/***nnti kito buat del sini kkg */}
                </View>
            </View>
        )
    }

    componentDidMount() {
        const url = 'http://1427987.iium.acme.my/api/product/read.php'
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    dataSource: responseJson.records
                })
            })
            .catch((error) => {
                console.log(error)
            })
    }
    render() {
        console.disableYellowBox = true;//remove the warning
        return (

            <View style={styles.container1}>
                <FlatList
                    data={this.state.dataSource}
                    renderItem={this.renderItem}
                    keyExtractor={(item, index) => index}

                />
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        backgroundColor: '#455a64',
        flex: 1,
        flexDirection: 'row',
    },
    container1: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
        margin: 10,
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    }
});
