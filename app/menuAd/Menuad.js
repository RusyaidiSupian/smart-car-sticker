import React, { Component } from 'react';
import {
  StyleSheet,
  View
} from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
import FormRegister from './FormRegister';
//import Profiles from '../components/Profiles';
import ViewData from './ViewData';
import DeleteData from './DeleteData';
import GenerateQr from './GenerateQr';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <ViewData />
      </View>
    );
  }
}

class Register extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <FormRegister />
      </View>
    );
  }
}
class Delete extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <DeleteData />
      </View>
    );
  }
}
class GenerateQR extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <GenerateQr />
      </View>
    );
  }
}

{/*class Profile extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Profiles />
      </View>
    );
  }
}*/}

class Menuad extends Component {
  static navigationOptions = { title: 'Menuad', header: null };
  render() {
    return (
      <UsersManager2 />
    );
  }
}


const UsersManager2 = createBottomTabNavigator({
  Home: { screen: HomeScreen },
  //Profile: { screen: Profile },
  Register: { screen: Register },
  Delete: { screen: Delete },
  GenerateQR: { screen: GenerateQR },
});

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
});

export default Menuad;