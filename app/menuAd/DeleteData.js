import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    TextInput,
    TouchableOpacity,
    ScrollView
} from 'react-native';

export default class FormRegister extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
        }
    }
    submit = () => {
        const { username } = this.state;
        fetch('http://1427987.iium.acme.my/api/product/delete.php', {
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if(responseJson.message === "Product was delete."){
                    alert(responseJson.message);
                }else{                   
                    alert(responseJson.message);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }
    render() {
        console.disableYellowBox = true;//remove the warning
        return (
            <View style={styles.container}>
                    <Text style={styles.buttonText}>Delete Data</Text>
                    {/* <View style={styles.container1}> */}
                        <TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Username"
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={(username) => this.setState({ username })}
                        />
                        <TouchableOpacity style={styles.button} onPress={this.submit} >
                            <Text style={styles.buttonText}>Enter Data</Text>
                        </TouchableOpacity>
                    {/* </View> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#455a64',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container1: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    inputBox: {
        width: 300,
        backgroundColor: 'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    }
});