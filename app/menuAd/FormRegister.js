import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    TextInput,
    TouchableOpacity,
    ScrollView,
    Picker
} from 'react-native';
import DatePicker from 'react-native-datepicker';

export default class FormRegister extends Component {

    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            name: '',
            contact: '',
            no_plat: '',
            resident_status: '',
            date_enrol: { date: "2018-11-10" },
        }
    }
    submit = () => {
        const { username, password, name, contact, no_plat, resident_status, date_enrol } = this.state;
        fetch('http://1427987.iium.acme.my/api/product/create.php', {
            method: 'POST',
            header: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                username: username,
                password: password,
                name: name,
                contact: contact,
                no_plat: no_plat,
                resident_status: resident_status,
                date_enrol: date_enrol
            })
        })
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson === "User was registerd." || responseJson === "Unable to register user") {
                    alert(responseJson.message);
                } else {
                    alert(responseJson.message);
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }
    render() {
        console.disableYellowBox = true;//remove the warning
        return (
            <View style={styles.container}>
                <ScrollView>
                    <Text style={styles.buttonText}>Register For User</Text>
                    <View style={styles.container1}>
                        <TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Username"
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={username => this.setState({ username })}
                        />
                        <TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Password"
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={password => this.setState({ password })}
                        />
                        <TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Name"
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={name => this.setState({ name })}
                        />
                        <TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="contact"
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={contact => this.setState({ contact })}
                        />
                        <TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Number Plat"
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={no_plat => this.setState({ no_plat })}
                        />
                        {/**<TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Resident or Admin"
                            placeholderTextColor="#ffffff"
                            selectionColor="#fff"
                            onChangeText={resident_status => this.setState({ resident_status })}
                        /> */}
                        <Picker
                            resident_status={this.state.resident_status}
                            style={{ height: 50, width: 100 }}
                            onValueChange={(resident_status) => { this.setState({ resident_status }) }}
                        >
                            <Picker.Item label="Resident" value="resident" />
                            <Picker.Item label="Admin" value="admin" />
                        </Picker>
                        <DatePicker
                            date_enrol={this.state.date_enrol}
                            mode="date"
                            placeholder="select date"
                            format="YYYY-MM-DD"
                            onDateChange={(date_enrol) => { this.setState({ date_enrol }) }}
                            confirmBtnText="Confirm"
                            cancelBtnText="Cancel"
                            customStyles={{
                                dateIcon: {
                                    position: 'absolute',
                                    left: 0,
                                    top: 4,
                                    marginLeft: 0
                                },
                                dateInput: {
                                    marginLeft: 36
                                }
                            }}
                        />
                        <TouchableOpacity style={styles.button} onPress={this.submit} >
                            <Text style={styles.buttonText}>Enter Data</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#455a64',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container1: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    inputBox: {
        width: 300,
        backgroundColor: 'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    }
});