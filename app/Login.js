import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    TextInput,
    TouchableOpacity,
    Keyboard,
    KeyboardAvoidingView,
    AsyncStorage,
} from 'react-native';
import Logo from './Logo';


class Login extends Component {
    static navigationOptions = { title: 'Login', header: null };//to hide navigation bar

    constructor() {
        super();
        this.state = {
            value: {
                username: '',
                password: '',
            }
        }
    }

    login = () => {

        const { username, password } = this.state;
        if (username == "") {
            alert('Please fill the username!');
        } else if (password == "") {
            alert('Please fill the password!');
        } else {
            fetch('http://1427987.iium.acme.my/api/product/login_admin.php', {
                method: 'POST',
                header: {
                    'Accept': 'application/json',
                    'Content-type': 'application/json'
                },
                body: JSON.stringify({
                    username: this.state.username,
                    password: this.state.password
                })
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    if (responseJson.message === "success") {
                        //alert(responseJson.message);
                        // AsyncStorage.setItem('username', res.username);
                        this.props.navigation.navigate("MenuAdmin", {UserName: this.state.username});
                        //alert("Succesfully login");

                    } else {
                        fetch('http://1427987.iium.acme.my/api/product/login_resident.php', {
                            method: 'POST',
                            header: {
                                'Accept': 'application/json',
                                'Content-type': 'application/json'
                            },
                            body: JSON.stringify({
                                username: this.state.username,
                                password: this.state.password
                            })
                        })
                        .then((response) => response.json())
                        .then((responseJson) => {
                            if(responseJson.message === "Success as Resident") {
                                this.props.navigation.navigate("MenuResident", {UserName: this.state.username});
                            }
                            else {
                                alert(responseJson.message);
                            }
                        })
                        .catch((error)=>{
                            console.error(error);
                        })
                    }
                
                })
                .catch((error) => {
                    console.error(error);
                });
        }
        Keyboard.dismiss();
    }

    render() {
        console.disableYellowBox = true;//remove the warning
        return (
            <View style={styles.container}>
                < Logo />
                <View style={styles.container1}>
                    <TextInput style={styles.inputBox}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Username"
                        placeholderTextColor="#ffffff"
                        selectionColor="#fff"
                        onChangeText={(username) => this.setState({ username })}
                    />
                    <TextInput style={styles.inputBox}
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholder="Password"
                        secureTextEntry={true}
                        placeholderTextColor="#ffffff"
                        //onChangeText={this.handlePassword}
                        onChangeText={(password) => this.setState({ password })}
                    //ref={(input) => this.password = input}
                    />
                    <TouchableOpacity style={styles.button} onPress={this.login} >
                        <Text style={styles.buttonText}>Login</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#455a64',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    container1: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    inputBox: {
        width: 300,
        backgroundColor: 'rgba(255, 255,255,0.2)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    button: {
        width: 300,
        backgroundColor: '#1c313a',
        borderRadius: 25,
        marginVertical: 10,
        paddingVertical: 13
    },
    buttonText: {
        fontSize: 16,
        fontWeight: '500',
        color: '#ffffff',
        textAlign: 'center'
    }
});

export default Login;