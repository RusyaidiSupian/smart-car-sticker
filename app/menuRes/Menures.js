import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  TextInput,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';
import Camera from 'react-native-camera';

// import CameraHome from './CameraHome';

class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      qrcode: ''

      //refreshing: false,
    }
    onBarCodeRead = (e) => this.setState({ qrcode: e.data });
  }
  render() {
    return (
      <View style={styles.container}>
        <Camera
          style={styles.preview}
          onBarCodeRead={this.onBarCodeRead}
          ref={cam => this.camera = cam}
          aspect={Camera.constants.Aspect.fill}
        >
          <Text style={{
            backgroundColor: 'white'
          }}>{this.state.qrcode}</Text>
        </Camera>
      </View>
    );
  }
}

class Profiles extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      dataSource: [],

      //refreshing: false,
    }
  }
  generate = () => {
    const { username } = this.state;
    fetch('http://1427987.iium.acme.my/api/product/readone.php', {
      method: 'POST',
      header: {
        'Accept': 'application/json',
        'Content-type': 'application/json'
      },
      body: JSON.stringify({
        username: username,
      })
    })
      .then((response) => response.json())
      .then((responseJson) => {
        this.setState({
          dataSource: responseJson.records
        });
      })
      .catch((error) => {
        console.log(error)
      })
    Keyboard.dismiss();
  }

  console() {
    alert(this.state.username)
  }
  //custom the layout of flatlist here
  renderItem = ({ item }) => {
    return (
      <View /**style={styles.container} warap with qrcode nnti*/>
        <View style={styles.container1} >
          <Text style={styles.buttonText}> Data {item.id} </Text>
          <Text>Username: {item.username}</Text>
          <Text>Name: {item.name}</Text>
          <Text>Contact: {item.contact}</Text>
          <Text>Car Number: {item.no_plat}</Text>
          <Text>Status: {item.resident_status}</Text>
          <Text>Date Enrollment: {item.date_enrol}</Text>
        </View>
      </View>
    )
  }
  render() {
    const { navigation } = this.props;
    const User = navigation.getParam('UserName');
    return (
      <View style={styles.container2}>
        <View style={styles.container1}>
          <Text style={styles.buttonText}>Profiles</Text>
          {/* <TextInput style={styles.inputBox}
            underlineColorAndroid='rgba(0,0,0,0)'
            placeholder="Username"
            placeholderTextColor="#ffffff"
            selectionColor="#fff"
            onChangeText={(username) => this.setState({ username })}
          /> */}
          <TouchableOpacity style={styles.button} onPress={this.console} >
            <Text style={styles.buttonText}>Generate</Text>
          </TouchableOpacity>
          <FlatList
            data={this.state.dataSource}
            renderItem={this.renderItem}
          //keyExtractor={(item, index) => index}     
          >
          </FlatList>


        </View>
      </View>
    );
  }
}

class Menures extends Component {
  static navigationOptions = { title: 'Menures', header: null };
  render() {

    return (
      <UsersManager1 />
    );
  }
}

const UsersManager1 = createBottomTabNavigator({
  Home: HomeScreen,
  Profiles: Profiles,
});

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#455a64',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  container2: {
    backgroundColor: '#455a64',
    flex: 1,
    flexDirection: 'row',
  },
  container1: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 30,
  },
  buttonText: {
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center'
  },
  inputBox: {
    width: 300,
    backgroundColor: 'rgba(255, 255,255,0.2)',
    borderRadius: 25,
    paddingHorizontal: 16,
    fontSize: 16,
    color: '#ffffff',
    marginVertical: 10
  },
  button: {
    width: 300,
    backgroundColor: '#1c313a',
    borderRadius: 25,
    marginVertical: 10,
    paddingVertical: 13
  },
  preview: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    height: 300,
    width: 300,
}
});


export default Menures;